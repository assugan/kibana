FROM ${DOCKER_KIBANA_IMAGE} as kibana

FROM ${DOCKER_FROM_IMAGE}

ENV KIBANA_ETC="/etc/kibana"

RUN mkdir -p \ 
     /var/log/kibana \
     /var/lib/kibana \
     /var/lib/kibana/plugins \
     /etc/kibana

COPY --from=kibana \
     /usr/share/kibana /usr/share/kibana/


RUN set -ex; \
       ln -s /usr/share/kibana/bin/kibana.server /bin 

ADD 	./config/kibana.yml /config/kibana.yml
